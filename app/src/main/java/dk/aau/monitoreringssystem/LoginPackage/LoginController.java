package dk.aau.monitoreringssystem.LoginPackage;

import android.os.AsyncTask;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginController {

    int userId;
    boolean isCitizen, isDone;
    int intToReturn;


    public int checkLogin(String[] userNameAndPassword){
        SQLLoginController sqlLoginController = new SQLLoginController();
        isDone = false;
        userId = 0;
        intToReturn = 0;
        sqlLoginController.execute(userNameAndPassword);

        while (!isDone){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return intToReturn;
    }


    public class SQLLoginController extends AsyncTask<String, Void, Void> {

        private static final String connectionUrl = "jdbc:mysql://db.course.hst.aau.dk:3306/hst_2019_19gr6406?autoReconnect=true&useSSL=false&user=hst_2019_19gr6406&password=yiaquohvohghiqualaix&serverTimezone=UTC";
        Connection connection;

        @Override
        protected Void doInBackground(String... strings) {
            System.out.println("doInBackground running...");
            try {
                System.out.println("Trying to connect");
                connectToSQLdb();
            } catch (SQLException e) {
                intToReturn = 1;
                isDone = true;
                e.printStackTrace();
            }

            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("select UserId, IsCitizen from tblLoginTest where UserName= ? and Password = ?;");
                preparedStatement.setString(1, strings[0]);
                preparedStatement.setString(2, strings[1]);
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {

                    userId = (int) rs.getInt("UserId");
                    isCitizen = rs.getBoolean("IsCitizen");
                    intToReturn = 4;
                    isDone = true;
                    return null;
                }


                intToReturn = 2;
                isDone = true;
            } catch (SQLException e) {
                intToReturn = 3;
                isDone = true;
                e.printStackTrace();
            }

            finally {
                try {
                    if (preparedStatement != null) { preparedStatement.close(); }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (connection != null) { connection.close(); }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        private void connectToSQLdb() throws SQLException {
            try{
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(connectionUrl);
            }
            catch (Exception e){
                System.out.println("Exception in connectToSQLdb");
                e.printStackTrace();
            }
        }
    }
}
