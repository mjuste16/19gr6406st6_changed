package dk.aau.monitoreringssystem.LoginPackage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import dk.aau.monitoreringssystem.HealthCareProfessionalPackage.MainViewHealthCareProfessional;
import dk.aau.monitoreringssystem.CitizenMainView;
import dk.aau.monitoreringssystem.R;

public class LoginView extends AppCompatActivity {

    EditText etUserName, etPassword;
    Button btnLogin, btnHackTheSystem;
    TextView tvDisplayMessage;
    LoginController loginController;
    int returnInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_view);

        loginController = new LoginController();

        etUserName = findViewById(R.id.etUserName);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnHackTheSystem = findViewById(R.id.btnHackTheSystem);
        tvDisplayMessage = findViewById(R.id.tvDisplayMessage);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvDisplayMessage.setText("Trying to log in...");
                String[] userNameAndPW = new String[2];
                userNameAndPW[0] = etUserName.getText().toString();
                userNameAndPW[1] = etPassword.getText().toString();

                returnInt = loginController.checkLogin(userNameAndPW);

                switch (returnInt){
                    case 1:
                        tvDisplayMessage.setText("Connection to SQL-db failed");
                        break;
                    case 2:
                        tvDisplayMessage.setText("Invalid password or username");
                        break;
                    case 3:
                        tvDisplayMessage.setText("Something else");
                        break;
                    case 4:
                        tvDisplayMessage.setText(String.valueOf(loginController.userId));
                        openMainPage(loginController.userId, loginController.isCitizen);
                        break;
                        default:
                            tvDisplayMessage.setText("Default switch case");
                            break;
                }
            }
        });

        btnHackTheSystem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginView.this, CitizenMainView.class);
                intent.putExtra("userId",212223);
                startActivity(intent);
            }
        });
    }

    private void openMainPage(Integer userId, boolean isCitizen){
        if (userId!= null){
            if (isCitizen == true){
                Intent mainScreenCitizenIntent = new Intent(LoginView.this, CitizenMainView.class);
                mainScreenCitizenIntent.putExtra("userId",userId);
                startActivity(mainScreenCitizenIntent);
            }
            else if (isCitizen == false){
                Intent mainScreenHCPIntent = new Intent(LoginView.this, MainViewHealthCareProfessional.class);
                mainScreenHCPIntent.putExtra("userId",userId);
                startActivity(mainScreenHCPIntent);
            }
        }
    }
}
