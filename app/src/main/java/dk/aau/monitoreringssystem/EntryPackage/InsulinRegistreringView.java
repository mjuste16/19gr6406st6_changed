package dk.aau.monitoreringssystem.EntryPackage;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.time.LocalDateTime;

import dk.aau.monitoreringssystem.R;

public class InsulinRegistreringView extends AppCompatActivity {

    EditText editTextInsulinDosis, editTextInsulinTime, editTextInsulinDate;
    Button btnInsulinConfirm, btnTestSQL, btnNow;
    EntryDataTypeController controller;
    TextView textViewTilTest;
    Spinner spinnerInsulinType;
    int userId;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insulin_registrering);

        Bundle bundle = getIntent().getExtras();
        userId = bundle.getInt("userId");

        editTextInsulinDosis = (EditText) findViewById(R.id.editTextInsulinDosis);
        editTextInsulinTime = (EditText) findViewById(R.id.editTextInsulinTime);
        editTextInsulinDate = (EditText) findViewById(R.id.editTextInsulinDate);
        btnInsulinConfirm = (Button) findViewById(R.id.btnInsulinConfirm);
        textViewTilTest = (TextView) findViewById(R.id.textViewTilTest);
        spinnerInsulinType = findViewById(R.id.spinnerInsulinType);
        btnNow = findViewById(R.id.btnNow);

        controller = new EntryDataTypeController();

        String[] insulinTypesSpinner = new String[]{"Meget-hurtigtvirkende insulin", "Hurtigtvirkende insulin", "Intermediærtvirkende insulin", "Langtidsvirkende insulin", "Blandingsinsulin"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, insulinTypesSpinner);
        spinnerInsulinType.setAdapter(adapter);

        btnInsulinConfirm.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v) {
                getInsulinInput();
            }
            });

        btnNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalDateTime ldt = LocalDateTime.now();
                String hour = String.valueOf(ldt.getHour());
                String minute = String.valueOf(ldt.getMinute());
                String date = ldt.getDayOfMonth() + "/" + ldt.getMonthValue();
                editTextInsulinDate.setText(date);
                editTextInsulinTime.setText(hour + ":" + minute);
            }
        });
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        private void getInsulinInput() {
            int retur;
            String doseString = editTextInsulinDosis.getText().toString();
            String insulinTime = editTextInsulinTime.getText().toString();
            String insulinDate = editTextInsulinDate.getText().toString();
            int insulinType = spinnerInsulinType.getSelectedItemPosition();
            if (insulinDate.isEmpty() || insulinTime.isEmpty() || doseString.isEmpty()){
                retur = 3;
            }
            else {
                retur = controller.registerInsulin(doseString, insulinDate, insulinTime, insulinType, userId);
            }

            switch (retur){
                case 1:
                    textViewTilTest.setText("Insulin registreret");
                    editTextInsulinDate.setText("");
                    editTextInsulinDosis.setText("");
                    editTextInsulinTime.setText("");
                    break;
                case 2:
                    textViewTilTest.setText("Dosis må ikke overstige 100");
                    break;
                case 3:
                    textViewTilTest.setText("Dato, årstal eller klokkeslæt er ikke indtastet korrekt");
                    break;
            }
        }
    }
