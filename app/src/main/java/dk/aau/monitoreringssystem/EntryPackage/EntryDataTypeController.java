package dk.aau.monitoreringssystem.EntryPackage;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.time.LocalDateTime;
import java.util.regex.PatternSyntaxException;

import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.BloodGlucoseDataType;
import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.InsulinDataType;
import dk.aau.monitoreringssystem.SqlControllerPackage.SQLdatabaseUploadController;

public class EntryDataTypeController {


    int returnMessage;


    public EntryDataTypeController(){

    }

    /**
     * Metode til registrering af insulin indtag samt tidspunkt og dato for dette. Kalder convertStringToLocalDate
     * @param doseString string med maengde af insulin
     * @param dateString string med dag, måned og aar
     * @param timeString string med tid, som time og minutter
     */

     @RequiresApi(api = Build.VERSION_CODES.O)
    public int registerInsulin(String doseString, String dateString, String timeString, int insulinType, int userId){

        returnMessage = 1;
        float dose = Float.parseFloat(doseString);
        LocalDateTime time = convertStringToLocalDate(dateString, timeString);
        if (time == null){
            return returnMessage = 3;
        }
         InsulinDataType insulinReg;

        if (dose<=100 && time.isBefore(LocalDateTime.now())){

            insulinReg = new InsulinDataType(time, dose, insulinType,userId);
        }
        else if (dose>100){
            return returnMessage = 2;
        }
        else {
            return returnMessage = 3;
        }

        SQLdatabaseUploadController sqLdatabaseController;

         sqLdatabaseController = new SQLdatabaseUploadController();
         sqLdatabaseController.execute(insulinReg);
         // returnMessage kan også blive sat i converStringToLocalDate
         return returnMessage;

     }

    /**
     * Metode til opdele string og inddele til år, måned og dag. Herunder kaldes funktion til validering af tidsinput
     * @param dateString input til at dele op i år, måned og dag
     * @param timeString input til at tjekke og registrer som time og minutter. Se også valderingTime
     * @return
     */

    @SuppressLint("NewApi")
    private LocalDateTime convertStringToLocalDate(String dateString, String timeString) {
        String year, month, day;
        if (dateString.isEmpty()) {
            // Vi fikser senere
            return LocalDateTime.now();
        }

        try {
            String[] idSplit = dateString.split("[-./]+", 3);
            if (idSplit[0].length() == 2) {
                day = idSplit[0];
            } else if (idSplit[0].length() == 1){
                day = "0"+idSplit[0];
            } else {
                System.out.println("day doesn't contain exactly one or two numbers");
                day = null;
            }

            if (idSplit[1].length() == 2) {
                month = idSplit[1];
            } else if (idSplit[1].length() == 1){
                month = "0"+idSplit[1];
            } else {
                System.out.println("month doesn't contain exactly one or two numbers");
                month = null;
            }

            if (idSplit.length == 3) {
                if (idSplit[2].length() == 4) {
                    year = idSplit[2];
                } else if (idSplit[2].length() == 2) {
                    year = "20" + idSplit[2];
                } else year = "2019";
            } else {
                year = "2019";
            }

            boolean isYearNumbers = year.matches("[0-9]{4}");
            boolean isMonthNumbers = month.matches("[0-9]{2}");
            boolean isDayNumbers = day.matches("[0-9]{2}");
            String time = validateInsulinTime(timeString);

            if (isYearNumbers && isMonthNumbers && isDayNumbers && !time.isEmpty()) {
                return LocalDateTime.parse(year + "-" + month + "-" + day + time);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Metode til at validere tidspunkt fra brugerindtastning
     * @param initTime er inputtet fra brugeren under tidspunkt
     * @return returnerer en string på formen "14-26", som time-minut. Null returneres, hvis inputtet ikke godkendes
     */

    @SuppressLint("NewApi")
    private String validateInsulinTime(String initTime) {

        String hour, minutes;
        if (initTime.isEmpty()) {
            return null;
        }

        if (initTime.matches("[0-9]{4}")) {
            hour = initTime.substring(0, 2);         ////Medtager de to første characters i en string på 1434 og returnerer 14
            minutes = initTime.substring(2);        //Fjerner de to forste characters i en string på 1434 og returnerer 34
            return hour + "-" + minutes;
        }

        try {
            String[] splitString = initTime.split("[-:.]+", 2);
            if (splitString.length == 2) {

                if(splitString[0].length()==1){
                    splitString[0] = "0" + splitString[0];
                }

                if(splitString[1].length()==1){
                    splitString[1] = "0" + splitString[1];
                }

                boolean isHourDigits = splitString[0].matches("[0-9]{2}");
                boolean isMinutesDigits = splitString[1].matches("[0-9]{2}");

                if (isHourDigits && isMinutesDigits) {
                    int hourInt = Integer.parseInt(splitString[0]);
                    int minuteInt = Integer.parseInt(splitString[1]);
                    if (hourInt < 24 && minuteInt < 60) {
                        hour = splitString[0];
                        minutes = splitString[1];
                        return "T" + hour + ":" + minutes;
                    }
                }
            }

            return null;


        } catch (PatternSyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
}
