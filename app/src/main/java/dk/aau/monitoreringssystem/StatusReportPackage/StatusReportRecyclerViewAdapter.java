package dk.aau.monitoreringssystem.StatusReportPackage;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import dk.aau.monitoreringssystem.R;

public class StatusReportRecyclerViewAdapter extends RecyclerView.Adapter<StatusReportRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "ReportRecyclerViewAdapt";

    private ArrayList<String> mImageNames;
    private ArrayList<String> mDay;
    private ArrayList<String> mImages;
    Context mContext;

    public StatusReportRecyclerViewAdapter(Context context, ArrayList<String> mImageNames, ArrayList<String> mImages, ArrayList<String> mDay) {
        this.mImageNames = mImageNames;
        this.mImages = mImages;
        this.mDay = mDay;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        Log.d(TAG, "onBindViewHolder: ");
        Glide.with(mContext)
                .asBitmap()
                .load(mImages.get(position))
                .into(holder.image);
        holder.imageName.setText(mImageNames.get(position));
        holder.whichDay.setText(mDay.get(position));
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Clicked on: " + mImageNames.get(position));

                Toast.makeText(mContext,mImageNames.get(position), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image;
        TextView imageName;
        TextView whichDay;
        RelativeLayout parentLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_listitem);
            imageName = itemView.findViewById(R.id.imageName_listitem);
            whichDay = itemView.findViewById(R.id.day_listitem);
            parentLayout = itemView.findViewById(R.id.parent_layout);

        }
    }
}

