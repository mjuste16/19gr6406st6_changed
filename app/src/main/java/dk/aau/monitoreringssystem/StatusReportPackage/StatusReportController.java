package dk.aau.monitoreringssystem.StatusReportPackage;

import java.time.LocalDateTime;
import java.util.ArrayList;


import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.MonitorizationData;
import dk.aau.monitoreringssystem.SqlControllerPackage.SqlDownloadController;

public class StatusReportController {

    private int userId;

    public SqlDownloadController insulinDownloadController;
    public SqlDownloadController bgDownloadController;

    public StatusReportController(int userId){
        this.userId = userId;
        insulinDownloadController = new SqlDownloadController(userId);
        bgDownloadController = new SqlDownloadController(userId);
    }


    public ArrayList<StatusReportNumberOfRegistrations> fillStatusReportArrayList(ArrayList<MonitorizationData> insulinArrayList, ArrayList<MonitorizationData> bgArrayList){

        ArrayList<StatusReportNumberOfRegistrations> statusReportNumberOfRegistrationsArrayList = new ArrayList<>();

        int sizeOfBGarray = bgArrayList.size();
        int sizeOfInsulinArray = insulinArrayList.size();

        LocalDateTime newestDay;
        LocalDateTime oldestDay;

        boolean bgArrayNotEmpty = true;
        boolean insulinArrayNotEmpty = true;

        //Tjekker om borger har registreret BG og insulin
        if (sizeOfBGarray==0 || sizeOfBGarray==0){
            if (sizeOfBGarray==0 && sizeOfInsulinArray==0){
                return statusReportNumberOfRegistrationsArrayList;
            }
            else if (sizeOfBGarray==0){
                bgArrayNotEmpty = false;
            }
            else {
                insulinArrayNotEmpty = false;
            }
        }

        if(bgArrayNotEmpty && insulinArrayNotEmpty){

            boolean insulinIsAfterBG_newest = insulinArrayList.get(0).getTimeOfRegistration().isAfter(bgArrayList.get(0).getTimeOfRegistration());
            boolean insulinIsAfterBG_oldest = insulinArrayList.get(sizeOfInsulinArray-1).getTimeOfRegistration().isAfter(bgArrayList.get(sizeOfBGarray-1).getTimeOfRegistration());

            if (insulinIsAfterBG_newest){
                newestDay = insulinArrayList.get(0).getTimeOfRegistration();
            }
            else {
                newestDay = bgArrayList.get(0).getTimeOfRegistration();
            }

            if (insulinIsAfterBG_oldest){
                oldestDay = bgArrayList.get(sizeOfBGarray-1).getTimeOfRegistration();
            }
            else {
                oldestDay = insulinArrayList.get(sizeOfInsulinArray-1).getTimeOfRegistration();
            }


        }
        else if (bgArrayNotEmpty){
            newestDay = bgArrayList.get(0).getTimeOfRegistration();
            oldestDay = bgArrayList.get(sizeOfBGarray-1).getTimeOfRegistration();
        }
        else {
            newestDay = insulinArrayList.get(0).getTimeOfRegistration();
            oldestDay = insulinArrayList.get(sizeOfInsulinArray-1).getTimeOfRegistration();
        }

        int noOfDaysBetweenNewestAndOldest = newestDay.compareTo(oldestDay);

        if (noOfDaysBetweenNewestAndOldest==1 && newestDay.getDayOfMonth()==oldestDay.getDayOfMonth()){
            noOfDaysBetweenNewestAndOldest=0;
        }


        for (int i=0; i<noOfDaysBetweenNewestAndOldest+1; i++){
            int noOfBG = 0;
            int noOfInsulin = 0;

            //Tjekker bgArrayList
            for (int j=0; j<sizeOfBGarray; j++){

                if (bgArrayList.get(j).getTimeOfRegistration().getMonthValue() == newestDay.minusDays(i).getMonthValue() && bgArrayList.get(j).getTimeOfRegistration().getDayOfMonth() == newestDay.minusDays(i).getDayOfMonth()) {

                    noOfBG++;
                }
            }

            //Tjekker insulinArrayList
            for (int j=0; j<sizeOfInsulinArray; j++){
                if (insulinArrayList.get(j).getTimeOfRegistration().getMonthValue() == newestDay.minusDays(i).getMonthValue() && insulinArrayList.get(j).getTimeOfRegistration().getDayOfMonth() == newestDay.minusDays(i).getDayOfMonth()) {
                    noOfInsulin++;
                }
            }
            System.out.println("noOfInsulin: " + noOfInsulin + ", noOfBG" + noOfBG);
            StatusReportNumberOfRegistrations srnr = new StatusReportNumberOfRegistrations(newestDay.minusDays(i),noOfInsulin, noOfBG);
            statusReportNumberOfRegistrationsArrayList.add(srnr);
        }

        return statusReportNumberOfRegistrationsArrayList;

    }

        public class StatusReportNumberOfRegistrations{
        private LocalDateTime localDateTime;
        private int numberOfInsulinReg;
        private int numberOfBGreg;

        StatusReportNumberOfRegistrations(LocalDateTime ldt, int noOfInsulin, int noOfBG){
            setLocalDateTime(ldt);
            setNumberOfInsulinReg(noOfInsulin);
            setNumberOfBGreg(noOfBG);
        }

        public LocalDateTime getLocalDateTime() {
            return localDateTime;
        }

        private void setLocalDateTime(LocalDateTime localDateTime) {
            this.localDateTime = localDateTime;
        }

        public int getNumberOfInsulinReg() {
            return numberOfInsulinReg;
        }

        private void setNumberOfInsulinReg(int numberOfInsulinReg) {
            this.numberOfInsulinReg = numberOfInsulinReg;
        }

        public int getNumberOfBGreg() {
            return numberOfBGreg;
        }

        private void setNumberOfBGreg(int numberOfBGreg) {
            this.numberOfBGreg = numberOfBGreg;
        }
    }


}
