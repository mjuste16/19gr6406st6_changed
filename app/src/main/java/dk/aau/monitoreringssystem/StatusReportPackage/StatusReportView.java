package dk.aau.monitoreringssystem.StatusReportPackage;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.time.LocalDateTime;
import java.util.ArrayList;

import dk.aau.monitoreringssystem.R;

public class StatusReportView extends AppCompatActivity {

    private static final String TAG = "ReportView";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mDay = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private int userId;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_report_view);

        Log.d(TAG, "onCreate: Started");

        Bundle bundle = getIntent().getExtras();
        userId = bundle.getInt("userId");
        System.out.println("USERID PASSED TO THIS VIEW: " + userId);

        initImageBitmaps();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: Preparing bitmaps");

        StatusReportController statusReportController = new StatusReportController(userId);
        statusReportController.bgDownloadController.execute(2);

        while (statusReportController.bgDownloadController.getIsDone()==false){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        statusReportController.insulinDownloadController.execute(1);

        while (statusReportController.insulinDownloadController.getIsDone()==false){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        ArrayList<StatusReportController.StatusReportNumberOfRegistrations> statusReportNumberOfRegistrationsArrayList;
        statusReportNumberOfRegistrationsArrayList = statusReportController.fillStatusReportArrayList(statusReportController.insulinDownloadController.getMonitorizationDataArrayList(), statusReportController.bgDownloadController.getMonitorizationDataArrayList());


        for (int i=0; i<statusReportNumberOfRegistrationsArrayList.size();i++){
            LocalDateTime ldt = statusReportNumberOfRegistrationsArrayList.get(i).getLocalDateTime();
            String dateString = String.valueOf(ldt.getDayOfMonth()) + "/" + String.valueOf(ldt.getMonthValue()) + "-" + String.valueOf(ldt.getYear());
                    mImageUrls.add("https://images-na.ssl-images-amazon.com/images/I/31iQU5%2ByoWL._SX355_.jpg");
            mNames.add("Antal registreringer for " + dateString + " er : " + statusReportNumberOfRegistrationsArrayList.get(i).getNumberOfInsulinReg());
            mDay.add("Insulin");

            mImageUrls.add("https://iconsgalore.com/wp-content/uploads/2018/10/blood-drop-1-featured-2.png");
            mNames.add("Antal registreringer for: " + dateString + " er : " + statusReportNumberOfRegistrationsArrayList.get(i).getNumberOfBGreg()); // + bloodGlucoseList.size());
            mDay.add("Blodglukose");
        }

        initRecyclerView();

        if (statusReportNumberOfRegistrationsArrayList.size()==0){
            System.out.println("SIZE == 0");
            setContentView(R.layout.activity_nodata);
        }
    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        StatusReportRecyclerViewAdapter recyclerViewAdapter = new StatusReportRecyclerViewAdapter(this, mNames, mImageUrls, mDay);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

}
