package dk.aau.monitoreringssystem.MonitorizationDataTypePackage;

import java.time.LocalDateTime;

public class BloodGlucoseDataType extends MonitorizationData{

    private float bloodGlucoseValue;

    public BloodGlucoseDataType(java.util.Date date, float bloodGlucoseValue, int userId) {
        super(date,userId);
        this.setBloodGlucoseValue(bloodGlucoseValue);
    }

    public BloodGlucoseDataType(LocalDateTime localDateTime, float bloodGlucoseValue, int userId){
        super(localDateTime,userId);
        setBloodGlucoseValue(bloodGlucoseValue);

    }

    public float getValueForDiagram() {
        return bloodGlucoseValue;
    }

    private void setBloodGlucoseValue(float bloodGlucoseValue) {
        if (bloodGlucoseValue > 0) {
            this.bloodGlucoseValue = bloodGlucoseValue;
      }
    }

}
