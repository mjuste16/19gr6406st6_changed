package dk.aau.monitoreringssystem.MonitorizationDataTypePackage;

public interface Plotable {
    float getValueForDiagram();
}
