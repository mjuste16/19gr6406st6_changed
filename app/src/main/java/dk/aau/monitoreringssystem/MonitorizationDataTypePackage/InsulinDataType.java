package dk.aau.monitoreringssystem.MonitorizationDataTypePackage;


import java.time.LocalDateTime;
import java.util.Date;

public class InsulinDataType extends MonitorizationData{
    private float dosis;
    private int insulinType;

    public InsulinDataType(LocalDateTime time, float dosis, int insulinType, int userId){
        super(time,userId);
        setDosis(dosis);
        this.insulinType = insulinType;
    }

    public InsulinDataType(Date date, float dosis, int insulinType, int userId) {
        super(date,userId);
        setDosis(dosis);
        this.insulinType = insulinType;
    }

    public float getValueForDiagram(){
        return getDosis();
    }

    public float getDosis() {
        return dosis;
    }

    private void setDosis(float dosis) {
        if (dosis>=0 && dosis<=100){
            this.dosis = dosis;
        }
    }

    public int getInsulinType() {
        return insulinType;
    }

}
