package dk.aau.monitoreringssystem.MonitorizationDataTypePackage;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public abstract class MonitorizationData implements Plotable{
    private LocalDateTime timeOfRegistration;
    private Date timeOfRegistrationDate;
    private int userId;

    public MonitorizationData(LocalDateTime timeOfRegistration, int userId){
        setTimeOfRegistration(timeOfRegistration);
        this.timeOfRegistrationDate = convertToDate(timeOfRegistration);
        this.userId=userId;

    }

    public MonitorizationData(Date timeOfRegistrationDate, int userId){
        setTimeOfRegistrationDate(timeOfRegistrationDate);
        this.timeOfRegistration = convertToLocalDateTime(timeOfRegistrationDate);
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public LocalDateTime getTimeOfRegistration() {
        return timeOfRegistration;
    }

    private void setTimeOfRegistration(LocalDateTime timeOfRegistration) {
            this.timeOfRegistration = timeOfRegistration;
    }

    public Date getTimeOfRegistrationAsDate() {
        return timeOfRegistrationDate;
    }

    private void setTimeOfRegistrationDate(Date timeOfRegistrationDate) {
        this.timeOfRegistrationDate = timeOfRegistrationDate;
    }

    private LocalDateTime convertToLocalDateTime(Date dateToConvert){
        Instant instant = dateToConvert.toInstant();
        LocalDateTime ldt = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
        return ldt;
    }

    private Date convertToDate(LocalDateTime ldtToConvert){
        long timeAsLong = toEpochMilli(ldtToConvert);
        Timestamp timestamp = new Timestamp(timeAsLong);
        return timestamp;
    }

    private long toEpochMilli(LocalDateTime localDateTime)
    {
        return localDateTime.atZone(ZoneId.systemDefault())
                .toInstant().toEpochMilli();
    }
}
