package dk.aau.monitoreringssystem.TransferPackage;

import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.MonitorizationData;
import dk.aau.monitoreringssystem.SqlControllerPackage.SqlDbSimulatedDataController;
import dk.aau.monitoreringssystem.SqlControllerPackage.SQLdatabaseUploadController;

public class TransferDataTypeController {

    int userId;

    MonitorizationData[] bgArray;

    private static final String connectionUrl = "jdbc:mysql://db.course.hst.aau.dk:3306/hst_2019_19gr6406?autoReconnect=true&useSSL=false&user=hst_2019_19gr6406&password=yiaquohvohghiqualaix";

    public TransferDataTypeController(int userId ){
        this.userId = userId;
    }

    public void getBloodGlucoseFromDatabase() {

        SqlDbSimulatedDataController sqlDbSimulatedDataController = new SqlDbSimulatedDataController(userId, null);
        sqlDbSimulatedDataController.execute(1);
        System.out.println("Faerdig med første execute");
        while (!sqlDbSimulatedDataController.getIsDone()){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Klar til anden");
        SqlDbSimulatedDataController sqlDbSimulatedDataController2 = new SqlDbSimulatedDataController(userId, sqlDbSimulatedDataController.getTimestampLastBGDate());
        sqlDbSimulatedDataController2.execute(2);

        while (!sqlDbSimulatedDataController2.getIsDone()){
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        bgArray = sqlDbSimulatedDataController2.getBgArray();

        if (bgArray!=null){
            System.out.println("Gik ind");
            SQLdatabaseUploadController sqLdatabaseUploadController = new SQLdatabaseUploadController();
            sqLdatabaseUploadController.execute(bgArray);
        }
    }
}










