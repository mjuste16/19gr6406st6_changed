package dk.aau.monitoreringssystem.TransferPackage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import dk.aau.monitoreringssystem.R;

public class BloodGlucoseRegistrationView extends AppCompatActivity {
    Button btnGetBloodGlucoseData;
    TextView txtData;
    TransferDataTypeController controller;
    int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood_glucose_registration_view);

        Bundle bundle = getIntent().getExtras();
        userId = bundle.getInt("userId");

        controller = new TransferDataTypeController(userId);
        txtData = this.findViewById(R.id.BloodGlucoseDataView);
        btnGetBloodGlucoseData = findViewById(R.id.GetBloodGlucoseData);

        btnGetBloodGlucoseData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.getBloodGlucoseFromDatabase();
                }

            });
    }
}
