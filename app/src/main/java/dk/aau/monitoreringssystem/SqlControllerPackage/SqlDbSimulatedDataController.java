package dk.aau.monitoreringssystem.SqlControllerPackage;

import android.os.AsyncTask;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.BloodGlucoseDataType;
import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.MonitorizationData;

public class SqlDbSimulatedDataController extends AsyncTask<Integer, Void, Void> {

    public ArrayList<BloodGlucoseDataType> bloodGlucoseList = new ArrayList<>();

    private static final String connectionUrl = "jdbc:mysql://db.course.hst.aau.dk:3306/hst_2019_19gr6406?autoReconnect=true&useSSL=false&user=hst_2019_19gr6406&password=yiaquohvohghiqualaix&serverTimezone=UTC";

    public SqlDbSimulatedDataController(int citizenUserId, Timestamp timestampLastBGDate){
        this.citizenUserId = citizenUserId;
        this.timestampLastBGDate = timestampLastBGDate;
    }

    private Timestamp timestampLastBGDate;
    private int citizenUserId; //Borgerens ID
    private MonitorizationData[] bgArray;

    private Boolean isDone = false; //Boolean til tjek af status paa SQL-query
    public String res = "";
    Connection connection;

    @Override
    protected Void doInBackground (Integer...integers){

        try {
            connectToSQLdb();

            isDone = getSimulatedBGdata(integers[0]);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void connectToSQLdb(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(connectionUrl);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean getSimulatedBGdata(int typeOfData) throws SQLException {
        Statement statement = connection.createStatement();

        switch (typeOfData) {
            // henter fra 'rigtig' tblBloodGlucose for at find den sidste dato der er blooglucose fra.
            case 1:
                ResultSet rsBloodGlucoseMaxTime = statement.executeQuery("select TidspunktBG from tblBloodGlucose where UserId = " + citizenUserId + " ORDER BY TidspunktBG DESC;");

                if (!rsBloodGlucoseMaxTime.next()){
                    Long tsNowLong = System.currentTimeMillis();
                    Long threeWeeksAgo = tsNowLong - 21*24*60*60*1000;
                    timestampLastBGDate = new Timestamp(threeWeeksAgo);
                }
                else {
                    timestampLastBGDate = rsBloodGlucoseMaxTime.getTimestamp("TidspunktBG");
                }
                break;
            case 2:
                ResultSet rsBloodGlucose = statement.executeQuery("select DATAVALUE, TIMEDATE from tblSimuleretBloodGlucose where TIMEDATE > '" + getTimestampLastBGDate() + "' order by TIMEDATE ASC");

                if (!rsBloodGlucose.next()){
                    break;
                }

                while (rsBloodGlucose.next()) {
                    Timestamp tempsqlTimeBGValue = rsBloodGlucose.getTimestamp("TIMEDATE");
                    java.util.Date tempTimeBGValue = new java.util.Date(tempsqlTimeBGValue.getTime());
                    float tempBloodGlucoseValue = rsBloodGlucose.getFloat("DATAVALUE");
                    BloodGlucoseDataType bloodGlucoseDataType = new BloodGlucoseDataType(new java.util.Date(tempsqlTimeBGValue.getTime()), tempBloodGlucoseValue,citizenUserId);
                    bloodGlucoseList.add(bloodGlucoseDataType);
                }
                bgArray = new MonitorizationData[bloodGlucoseList.size()];
                for (int i=0; i<bloodGlucoseList.size(); i++){
                    bgArray[i] = bloodGlucoseList.get(i);
                }
                bloodGlucoseList.clear();
                break;
        }
        connection.close();
        return true;
    }


    public Timestamp getTimestampLastBGDate() {
        return timestampLastBGDate;
    }

    public MonitorizationData[] getBgArray() {
        return bgArray;
    }

    public Boolean getIsDone() {
        return isDone;
    }

}
