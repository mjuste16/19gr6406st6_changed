package dk.aau.monitoreringssystem.SqlControllerPackage;

import android.os.AsyncTask;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;

import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.BloodGlucoseDataType;
import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.InsulinDataType;
import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.MonitorizationData;

public class SqlDownloadController extends AsyncTask<Integer, Void, Void> {
    private static final String connectionUrl = "jdbc:mysql://db.course.hst.aau.dk:3306/hst_2019_19gr6406?autoReconnect=true&useSSL=false&user=hst_2019_19gr6406&password=yiaquohvohghiqualaix&serverTimezone=UTC";

    private boolean isDone = false;
    private int userId;

    Connection connection;
    private ArrayList<MonitorizationData> monitorizationDataArrayList = new ArrayList<>();

    public SqlDownloadController(int userId){
        this.userId = userId;
    }


    @Override
    protected Void doInBackground(Integer... integers) {

        try {
            connectToSQLdb();

            isDone = getInsulinOrBGregistrations(integers[0]);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void connectToSQLdb() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(connectionUrl);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param typeOfData, 1 for insulin, and 2 for blood glucose
     * @return isDone=true, when the method is done
     * @throws SQLException
     */

    public boolean getInsulinOrBGregistrations(int typeOfData) throws SQLException {
        Statement statement = connection.createStatement();
        Long tsNowLong = System.currentTimeMillis();
        Long threeWeeksAgo = tsNowLong - 21*24*60*60*1000;
        Timestamp timestampThreeWeeks = new Timestamp(threeWeeksAgo);
        Timestamp timestampNow = new Timestamp(tsNowLong);

        switch (typeOfData) {
            case 1:

                ResultSet rsInsulin = statement.executeQuery("select * from tblInsulinTest where UserId=" + userId + " AND Tidspunkt BETWEEN '" + timestampThreeWeeks.toString() + "' AND '" + timestampNow.toString() + "' ORDER BY Tidspunkt DESC;");

                while (rsInsulin.next()) {

                    Timestamp timestamp = rsInsulin.getTimestamp("Tidspunkt");
                    Long longValue = timestamp.getTime();
                    LocalDateTime ldt =
                            LocalDateTime.ofInstant(Instant.ofEpochMilli(longValue), ZoneId.systemDefault());

                    float dose = rsInsulin.getFloat("Dosis");
                    int insulinType = rsInsulin.getInt("InsulinType");
                    int userId = rsInsulin.getInt("UserId");
                    InsulinDataType insulin = new InsulinDataType(ldt, dose, insulinType, userId);
                    getMonitorizationDataArrayList().add(insulin);
                }
                break;

            case 2:

                ResultSet rsBloodGlucose = statement.executeQuery("select * from tblBloodGlucose where UserId=" + userId + " AND TidspunktBG BETWEEN '" + timestampThreeWeeks.toString() + "' AND '" + timestampNow.toString() + "' ORDER BY TidspunktBG DESC;");

                while (rsBloodGlucose.next()) {

                    Timestamp timestamp = rsBloodGlucose.getTimestamp("TidspunktBG");
                    Long longValue = timestamp.getTime();
                    LocalDateTime ldt =
                            LocalDateTime.ofInstant(Instant.ofEpochMilli(longValue), ZoneId.systemDefault());
                    float bgValue = rsBloodGlucose.getFloat("BGvalue");
                    int bgUserId = rsBloodGlucose.getInt("UserId");
                    BloodGlucoseDataType bloodGlucose = new BloodGlucoseDataType(ldt, bgValue, bgUserId);
                    getMonitorizationDataArrayList().add(bloodGlucose);
                }

                break;

            default:
                System.out.println("Integer passed to .execute not compatible");
        }

        statement.close();
        connection.close();

        return true;
    }

    public ArrayList<MonitorizationData> getMonitorizationDataArrayList() {
        return monitorizationDataArrayList;
    }

    public Boolean getIsDone() {
        return isDone;
    }
}
