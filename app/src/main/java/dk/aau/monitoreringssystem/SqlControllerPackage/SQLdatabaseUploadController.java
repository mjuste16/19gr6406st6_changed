package dk.aau.monitoreringssystem.SqlControllerPackage;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;

import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.BloodGlucoseDataType;
import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.InsulinDataType;
import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.MonitorizationData;

public class SQLdatabaseUploadController extends AsyncTask<MonitorizationData, Void, Void> {
    private static final String connectionUrl = "jdbc:mysql://db.course.hst.aau.dk:3306/hst_2019_19gr6406?autoReconnect=true&useSSL=false&user=hst_2019_19gr6406&password=yiaquohvohghiqualaix&serverTimezone=UTC";

    Connection connection;

    java.util.Date dateUtil;
    java.sql.Date dateSQL;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected Void doInBackground(MonitorizationData... params) {
        try{
            connectToSQLdb();
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("Couldn't connect to database");
        }

        if (params[0].getClass().equals(InsulinDataType.class)){
            try {
                uploadInsulinToSqlDb((InsulinDataType) params[0]);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else if (params[0].getClass().equals(BloodGlucoseDataType.class)){
            int arraySize = params.length;

            for (int i = 0; i<arraySize; i++){
                try {
                    uploadBgToSqlDb((BloodGlucoseDataType) params[i]);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }



    public void connectToSQLdb() throws SQLException {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(connectionUrl);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void uploadInsulinToSqlDb(InsulinDataType insulinDataType) throws SQLException {

        String dose = Float.toString(insulinDataType.getDosis());
        LocalDateTime ldt = insulinDataType.getTimeOfRegistration();
        long longTimeNoSee = toEpochMilli(ldt);
        Timestamp testTid = new Timestamp(longTimeNoSee);

        Statement statement = connection.createStatement();
        statement.execute("INSERT INTO tblInsulinTest (Dosis, Tidspunkt, InsulinType, UserId) VALUES ("+ dose +", '" + testTid +"', " + insulinDataType.getInsulinType() + ", " + insulinDataType.getUserId() + ");");
        connection.close();
    }

    private void uploadBgToSqlDb(BloodGlucoseDataType bloodGlucoseDataType) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO tblBloodGlucose (BGvalue, TidspunktBG, UserId) VALUES (?, ?, ?)");

            float bgValue = bloodGlucoseDataType.getValueForDiagram();
            int bgUserId = bloodGlucoseDataType.getUserId();
            dateUtil = bloodGlucoseDataType.getTimeOfRegistrationAsDate();
            Timestamp timestamp = new Timestamp(dateUtil.getTime());

            preparedStatement.setFloat(1, bgValue);
            preparedStatement.setTimestamp(2, (timestamp));
            preparedStatement.setInt(3, bgUserId);
            preparedStatement.execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private long toEpochMilli(LocalDateTime localDateTime)
    {
        return localDateTime.atZone(ZoneId.systemDefault())
                .toInstant().toEpochMilli();
    }

}
