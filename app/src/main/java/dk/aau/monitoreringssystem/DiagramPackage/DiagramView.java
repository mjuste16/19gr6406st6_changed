package dk.aau.monitoreringssystem.DiagramPackage;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.jjoe64.graphview.series.Series;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.MonitorizationData;
import dk.aau.monitoreringssystem.R;

public class DiagramView extends AppCompatActivity {

    GraphView diagram;
    DiagramController diagramController;
    Switch swInsulin;
    Switch swBloodGlucose;
    TextView txtInsulin;


    LineGraphSeries<DataPoint> bgSeries;
    PointsGraphSeries<DataPoint> insulinSeries;


    //Til Toasts
    DecimalFormat df2 = new DecimalFormat("#.##");
    SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        final int citizenUserId = bundle.getInt("userId");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagram_view);

        diagram = this.findViewById(R.id.Diagram);

        swInsulin = this.findViewById(R.id.swInsulin);

        swBloodGlucose = this.findViewById(R.id.swBloodGlucose);

        txtInsulin = this.findViewById(R.id.txtInsulin);

        // set date label formatter
        diagram.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this,new SimpleDateFormat("dd/MM HH:mm")));


        DiagramController diagramController = new DiagramController(citizenUserId);

        diagramController.bgDownloadController.execute(2);

        while (diagramController.bgDownloadController.getIsDone()==false){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        diagramController.insulinDownloadController.execute(1);

        while (diagramController.insulinDownloadController.getIsDone()==false){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        if (diagramController.bgDownloadController.getMonitorizationDataArrayList().size() == 0) {
            setContentView(R.layout.activity_nodata);
        }
        else  {
            ArrayList<MonitorizationData> bgArrayList = diagramController.bgDownloadController.getMonitorizationDataArrayList();
            ArrayList<MonitorizationData> insulinArrayList = diagramController.insulinDownloadController.getMonitorizationDataArrayList();

            bgSeries = new LineGraphSeries<>(diagramController.createDataPointArray(bgArrayList));  //initializing/defining bgSeries to get the data from the method 'data()'
            bgSeries.setTitle("Blodglukose");
            insulinSeries = new PointsGraphSeries<>(diagramController.createDataPointArray(insulinArrayList));
            insulinSeries.setTitle("Insulin");

            swBloodGlucose.setChecked(true);        //saa switch starter som taendt
            diagram.addSeries(bgSeries);          //saa blodglukose starter med at vaere plottet paa grafen
            diagram.getGridLabelRenderer().setVerticalAxisTitle("Blodglukose (mmol/l)"); //Titel til blod glukose aksen
            diagram.getGridLabelRenderer().setVerticalAxisTitleTextSize(40);
            diagram.getGridLabelRenderer().setVerticalAxisTitleColor(Color.BLACK);
            diagram.getLegendRenderer().setVisible(true);
            diagram.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.BOTTOM); //saetter en legend til vores datatyper


            bgSeries.setOnDataPointTapListener(new OnDataPointTapListener() {    //Toast til BG
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Date d = new Date((long) dataPoint.getX());
                    String stDato = format1.format(d.getTime());
                    Toast.makeText(DiagramView.this,df2.format(dataPoint.getY())+" mmol/l "+" tid: "+ stDato, Toast.LENGTH_LONG).show();
                }
            });

            insulinSeries.setOnDataPointTapListener(new OnDataPointTapListener() { //til toast
                @Override
                public void onTap(Series barSeries, DataPointInterface dataPoint) {
                    Date d = new Date((long) dataPoint.getX());
                    String stDato = format1.format(d.getTime());
                    Toast.makeText(DiagramView.this,df2.format(dataPoint.getY())+" Units"+" tid: "+ stDato, Toast.LENGTH_LONG).show();
                }
            });


            diagram.getViewport().setScalable(true); // enables horizontal zooming and scrolling
            diagram.getViewport().setScalableY(true); // enables vertical zooming and scrolling

            diagram.getGridLabelRenderer().setLabelsSpace(18); //Sets vertical space from the horizontal axis to labels for the axis
            LocalDateTime first, last;
            first = bgArrayList.get(bgArrayList.size()-1).getTimeOfRegistration();
            last = bgArrayList.get(0).getTimeOfRegistration();

            diagram.getGridLabelRenderer().setHorizontalLabelsAngle(145);

            // styling bgSeries

            bgSeries.setColor(Color.RED);
            bgSeries.setDrawDataPoints(true);
            bgSeries.setDataPointsRadius(15);
            bgSeries.setThickness(8);
            diagram.setTitle("Diagram over monitoreret data");
            diagram.setTitleTextSize(52);

            // set manual x bounds to have nice steps
            diagram.getViewport().setXAxisBoundsManual(true);
            diagram.getViewport().setMaxX(bgArrayList.get(0).getTimeOfRegistrationAsDate().getTime()+1000*60*60*18); // plus 18 timer
            diagram.getViewport().setMinX(bgArrayList.get(bgArrayList.size()-1).getTimeOfRegistrationAsDate().getTime());  //Skal aendres til at hente fra voresa ArrayList

            diagram.getViewport().setYAxisBoundsManual(true); //saettes true hvis y aksen skal være med fast min og max
            diagram.getViewport().setMinY(0); //Skal aendres til at hente fra voresa ArrayList

            swInsulin.setVisibility(View.VISIBLE);
        }


        swBloodGlucose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                if (isChecked) {
                    diagram.addSeries(bgSeries);  //adding the bgSeries to the GraphView
                    diagram.getGridLabelRenderer().setVerticalAxisTitle("Blod glukose (mmol/l)"); //Titel til blod glukose aksen
                    diagram.getGridLabelRenderer().setVerticalAxisTitleTextSize(40);
                    diagram.getGridLabelRenderer().setVerticalAxisTitleColor(Color.BLACK);

                                }
                else {
                    diagram.removeSeries(bgSeries);
                    diagram.getGridLabelRenderer().setVerticalAxisTitle(""); //Titel til blod glukose aksen

                }
                            }




        });

        swInsulin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    diagram.getSecondScale().addSeries(insulinSeries);

                    insulinSeries.setColor(Color.argb(250,20,0,200));
                    insulinSeries.setShape(PointsGraphSeries.Shape.RECTANGLE);
                    insulinSeries.setSize(15f);
                    txtInsulin.setVisibility(View.VISIBLE);

                    diagram.getSecondScale().setMinY(0);
                    diagram.getSecondScale().setMaxY(50);
                }
                else {
                    diagram.removeSeries(insulinSeries);
                    diagram.clearSecondScale();
                    txtInsulin.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
}
