package dk.aau.monitoreringssystem.DiagramPackage;

import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;
import java.util.Date;

import dk.aau.monitoreringssystem.MonitorizationDataTypePackage.MonitorizationData;
import dk.aau.monitoreringssystem.SqlControllerPackage.SqlDownloadController;

public class DiagramController {

    private int citizenUserId; //Borgerens ID
    Boolean isDone = false; //Boolean til tjek af status paa SQL-query

    SqlDownloadController insulinDownloadController;
    SqlDownloadController bgDownloadController;

    public DiagramController(int citizenUserId){
        this.citizenUserId = citizenUserId;
        insulinDownloadController = new SqlDownloadController(citizenUserId);
        bgDownloadController = new SqlDownloadController(citizenUserId);
    }



    /**
     *
     * @return et array a typen DataPoint fra GraphView-library, indeholdende hentede datapunkter indeholdt i en arraylist.
     * Arraeyet anvendes til at plotte i DiagramView. Loekken er bagvendt, fordi sql-query henter registreringer i descending order.
     */
    public DataPoint[] createDataPointArray(ArrayList<MonitorizationData> arrayList){

        int sizeOfArray = arrayList.size();
        DataPoint[] values = new DataPoint[sizeOfArray];//creating an object of type DataPoint[] of size 'sizeOfArray'

        for (int i = 1; i < sizeOfArray+1; i++) {
            Date date = arrayList.get(sizeOfArray-i).getTimeOfRegistrationAsDate();
            float value = arrayList.get(sizeOfArray-i).getValueForDiagram();
            values[i-1] =  new DataPoint(date, value);
        }
        return values;
    }

}
