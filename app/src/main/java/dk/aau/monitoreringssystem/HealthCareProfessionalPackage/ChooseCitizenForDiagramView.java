package dk.aau.monitoreringssystem.HealthCareProfessionalPackage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

import dk.aau.monitoreringssystem.DiagramPackage.DiagramView;
import dk.aau.monitoreringssystem.R;

public class ChooseCitizenForDiagramView extends AppCompatActivity {

    ChooseCitizenController controller;

    EditText etUserId;
    Button btnCheckUserId;
    TextView tvDialog;

    int returnedUserId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_citizen_for_diagram);

        controller = new ChooseCitizenController();

        etUserId = findViewById(R.id.etUserId);
        btnCheckUserId = findViewById(R.id.btnCheckUserId);
        tvDialog = findViewById(R.id.tvDialog);

        btnCheckUserId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userIdInput = etUserId.getText().toString();

                try {
                    passUserIdToController(userIdInput);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
    }

    private void openActivityDiagramView(int citizenId){
        Intent intent = new Intent(this, DiagramView.class);
        intent.putExtra("userId",citizenId);
        startActivity(intent);
    }

    private Void passUserIdToController(String inputId) throws ExecutionException, InterruptedException {
        returnedUserId = 0;
        if (inputId.isEmpty()){
            tvDialog.setText("No user id entered");
        }
        else {
            controller.checkUserId(inputId);
            tvDialog.setText("Checking user id: " + inputId);
            while (controller.sqlCheckDone == false) {
                Thread.sleep(100);
            }
            controller.sqlCheckDone = false;
            returnedUserId = controller.confirmedUserId;
            if (returnedUserId != 0){
                openActivityDiagramView(returnedUserId);
            }
            else {
                tvDialog.setText("User id couldn't be found - or exception was thrown");
            }
        }
        return null;
    }
}
