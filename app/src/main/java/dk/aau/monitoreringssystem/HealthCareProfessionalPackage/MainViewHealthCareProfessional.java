package dk.aau.monitoreringssystem.HealthCareProfessionalPackage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import dk.aau.monitoreringssystem.LoginPackage.LoginView;
import dk.aau.monitoreringssystem.R;

public class MainViewHealthCareProfessional extends AppCompatActivity {

    Button btnDiagram, btnLogOutHCP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_view_health_care_professional);

        Bundle bundle = getIntent().getExtras();
        int userId = bundle.getInt("userId");

        btnDiagram = findViewById(R.id.btnDiagram);
        btnLogOutHCP = findViewById(R.id.btnLogOutHCP);

        btnDiagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityDiagramView();
            }
        });

        btnLogOutHCP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityLogin();
            }
        });
    }

    private void openActivityLogin(){
        Intent intent = new Intent(this, LoginView.class);
        startActivity(intent);
        finish();
    }

    private void openActivityDiagramView(){
        Intent intent = new Intent(this, ChooseCitizenForDiagramView.class);
        startActivity(intent);
    }
}
