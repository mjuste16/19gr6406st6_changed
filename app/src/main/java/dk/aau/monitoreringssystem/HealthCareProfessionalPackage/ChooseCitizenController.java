package dk.aau.monitoreringssystem.HealthCareProfessionalPackage;

import android.os.AsyncTask;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ChooseCitizenController {

    int confirmedUserId;
    boolean sqlCheckDone=false;


    public void checkUserId(String inputId) {
        confirmedUserId = 0;
        ChooseCitizenSqlCheck chooseCitizenSqlCheck = new ChooseCitizenSqlCheck();
        chooseCitizenSqlCheck.execute(inputId);
    }

    public class ChooseCitizenSqlCheck extends AsyncTask<String, Void, Void>{

        private static final String connectionUrl = "jdbc:mysql://db.course.hst.aau.dk:3306/hst_2019_19gr6406?autoReconnect=true&useSSL=false&user=hst_2019_19gr6406&password=yiaquohvohghiqualaix&serverTimezone=UTC";
        Connection connection;

        @Override
        protected Void doInBackground(String... strings) {
                try {
                    System.out.println("Trying to connect");
                    connectToSQLdb();
                } catch (SQLException e) {
                    System.out.println("Connection to SQL-db failed");
                    e.printStackTrace();
                    sqlCheckDone = true;
                }

                PreparedStatement preparedStatement = null;

            try {
                preparedStatement = connection.prepareStatement("select UserId from tblLoginTest where UserId= ? and IsCitizen = 1;");
                preparedStatement.setString(1, strings[0]);
                ResultSet rs = preparedStatement.executeQuery();
                int userId;
                while (rs.next()){
                    userId = rs.getInt("UserId");
                    confirmedUserId = userId;
                    sqlCheckDone = true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                sqlCheckDone = true;
            }
            sqlCheckDone = true;
            return null;
        }

        private void connectToSQLdb() throws SQLException {
            try{
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(connectionUrl);
            }
            catch (Exception e){
                System.out.println("Exception in connectToSQLdb");
                e.printStackTrace();
                sqlCheckDone = true;
            }
        }
    }
}
