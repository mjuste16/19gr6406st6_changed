package dk.aau.monitoreringssystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import dk.aau.monitoreringssystem.EntryPackage.InsulinRegistreringView;
import dk.aau.monitoreringssystem.TransferPackage.BloodGlucoseRegistrationView;
import dk.aau.monitoreringssystem.DiagramPackage.DiagramView;
import dk.aau.monitoreringssystem.LoginPackage.LoginView;
import dk.aau.monitoreringssystem.StatusReportPackage.StatusReportView;


public class CitizenMainView extends AppCompatActivity {


    Button btnTest, btnInsulinReg, btnBloodGlucose, btnSeDiagram, btnStatusrapport, btnLogOutCitizen;
    int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle bundle = getIntent().getExtras();
        userId = bundle.getInt("userId");

        btnInsulinReg = (Button) findViewById(R.id.btnInsulinReg);
        btnBloodGlucose = this.findViewById(R.id.btnBloodglucose);
        btnSeDiagram = this.findViewById(R.id.btnSeDiagram);
        btnStatusrapport = this.findViewById(R.id.btnStatusrapport);
        btnLogOutCitizen = findViewById(R.id.btnLogOutCitizen);

        btnInsulinReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityInsulinReg(userId);
            }
        });

        btnBloodGlucose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBloodGlucoseRegistrationView(userId);
            }
        });

        btnSeDiagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityDiagramView(userId);
            }
        });

        btnStatusrapport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStatusReportView(userId);
            }
        });

        btnLogOutCitizen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityLogin();
                finish();
            }
        });

    }

    private void openActivityDiagramView(int citizenId){
        Intent intent = new Intent(this, DiagramView.class);
        intent.putExtra("userId",citizenId);
        startActivity(intent);
    }

    private void openActivityInsulinReg(int userId){
        Intent intent = new Intent(this, InsulinRegistreringView.class);
        intent.putExtra("userId",userId);
        startActivity(intent);
    }

    private void openBloodGlucoseRegistrationView(int userId){
        Intent intent = new Intent(this, BloodGlucoseRegistrationView.class);
        intent.putExtra("userId",userId);
        startActivity(intent);
    }


    private void openStatusReportView(int userId){
        Intent intent = new Intent(this, StatusReportView.class);
        intent.putExtra("userId",userId);
        startActivity(intent);
    }

    private void openActivityLogin(){
        Intent intent = new Intent(this, LoginView.class);
        startActivity(intent);
        finish();
    }
}
